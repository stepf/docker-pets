# Docker Pets

This repo was forked from https://github.com/mark-church/docker-pets and modified to demonstrate how to use Gitlab CI with swarm or kubernetes. Please head over to the original repo to learn more about the code itself and how to use it with Docker datacenter.

# Demo

## Docker Recap

### Images

```bash
docker image build --tag registry.gitlab.com/stepf/docker-pets:1.0 .
docker image push registry.gitlab.com/stepf/docker-pets:1.0
```

### Containers
```bash
# run an application container
# open localhost:5000 in your browser and see what our application does
docker container run \
  --detach \
  --publish 5000:5000 \
  --name docker-pets \
  registry.gitlab.com/stepf/docker-pets:1.0

# cleanup
docker container rm --force docker-pets
```

### Volumes

Run a development containers with docker volumes:

```bash
# modify src/app.py, e.g. version (line 20)

# run an application container with ./src mounted
# observe on localhost:5000 that your changes are included
docker container run \
  --detach \
  --publish 5000:5000 \
  --name docker-pets \
  --volume $(pwd)/src:/app \
  registry.gitlab.com/stepf/docker-pets:1.0

# cleanup
docker container rm --force docker-pets
```

### Networking

Connecting our application to a database via docker network:

```bash
# variables containing settings
export DOCKER_NETWORK_NAME='docker-pets'
export APP_CONTAINER_NAME='docker-pets'
export DB_CONTAINER_NAME='docker-pets-db'

# create a custom bridge network
docker network create ${DOCKER_NETWORK_NAME}

# run a consul container (key value database) connected to the network
docker container run \
  --detach \
  --env CONSUL_BIND_INTERFACE='eth0' \
  --env CONSUL_LOCAL_CONFIG='{"skip_leave_on_interrupt": true}' \
  --name ${DB_CONTAINER_NAME} \
  --net ${DOCKER_NETWORK_NAME} \
  --publish 8500:8500 \
  consul:1.1.0 \
  agent -server -ui -client=0.0.0.0 -bootstrap-expect=1 -retry-join=${DB_CONTAINER_NAME} -retry-interval 5s

# run an application container connected to the network
# set the DB environment variable to 'docker-pets-db'
# observe on localhost:5000 how the application got stateful. yay!
docker container run \
  --detach \
  --env DB=${DB_CONTAINER_NAME} \
  --name ${APP_CONTAINER_NAME} \
  --net ${DOCKER_NETWORK_NAME} \
  --publish 5000:5000 \
  --volume $(pwd)/src:/app \
  registry.gitlab.com/stepf/docker-pets:1.0

# cleanup
docker container rm --force ${APP_CONTAINER_NAME} ${DB_CONTAINER_NAME}
docker network rm ${DOCKER_NETWORK_NAME}
```

### Putting it together: Full development workflow (without compose)

Ask yourself whether this is really a feasible workflow:

1. Create network
2. Start database
3. Start app with `./src` mounted
4. Develop
5. Build Image
6. Push Image
7. Repeat until done
8. Cleanup

## Docker Compose

Doing all the above with Docker Compose

```bash
# create images / networks, start containers, mount volumes
docker-compose up --detach

# stop all containres
docker-compose stop

# cleanup: networks. volumes, containers
docker-compose down

# build and push images
docker-compose build
docker-compose push
```

## Docker Swarm
```bash
# deploy
docker stack deploy \
  --compose-file docker-compose.yml \
  --compose-file swarm/docker-compose.prod.yml \
  docker-pets

# cleanup
docker stack rm docker-pets
```

## Kubernetes

```bash
# deploy consul
kubectl apply --filename kubernetes/consul-deployment.yml
kubectl apply --filename kubernetes/consul-service.yml

# deploy application
kubectl apply --filename kubernetes/app-deployment.yml
kubectl apply --filename kubernetes/app-service.yml

# cleanup
kubectl delete deployments docker-pets-db docker-pets
kubectl delete services docker-pets-db docker-pets
```

## Gitlab CI

```bash
# fill with meaningfull credentials after copying
cp gitlab-runner/config.example.toml gitlab-runner/config.toml

# setup your pipeline after copying
cp gitlab-runner/gitlab-ci-example.yml .gitlab-ci.yml

# run local gitlab runner (docker executor)
docker run --detach --name gitlab-runner --restart always \
  --volume $(pwd)/gitlab-runner/:/etc/gitlab-runner \
  --volume /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest

# push changes and watch your jobs get run
```
